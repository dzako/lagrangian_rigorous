/* CAPD integration

*/

#include "capd/capdlib.h"
#include <capd/vectalg/Vector_inline.h>
#include <capd/matrixAlgorithms/floatMatrixAlgorithms.hpp>

using namespace capd;
using namespace std;

typedef capd::dynsys::Solver<IMap> OdeSolver;

/* this flag tells if a Lagrangian update should be performed. It means that when a condition is satisfied
 * the set to be integrated in time is updated to be a representable enclosure for the lagragrangian  ball bounds,
 * this helps when the lagrangian bounds are tighter than the direct bounds. 
*/
const bool LAGRANGIAN_UPDATE = true;


interval computeRadius(const IVector& phiBound, int size, interval sf, interval previousR){
	interval maxDiam = 0.;
	for(int i=0; i < size; i++){
		if( diam(phiBound)[i] > maxDiam )
			maxDiam = diam(phiBound)[i];
	}
	interval newR = sqrt(interval(2.)) * maxDiam + (previousR * sf) ;
	return newR.rightBound();
}

//computes eigenvalues for a POSITIVE DEFINITE matrix m (currently only 2\times 2 matrices are implemented
void computeEigvPD(const IMatrix& m, interval& eig1, interval& eig2){
	interval delta2 = (m[0][0] + m[1][1]) * (m[0][0] + m[1][1]) - 4 * (m[0][0] * m[1][1] - m[0][1] * m[1][0]);
	interval stretch2;
	
	if(delta2 >= 0.){
		
	}else{
		//we may set delta2 leftBound to 0., as we asume about the set of matrices (Cauchy-Green stress tensor)
		//that they are symmetric.
		delta2.setLeftBound(0.);
	}
	eig1 = ( m[0][0] + m[1][1] - sqrt(delta2) ) / 2. ;
	if(!(eig1 >= 0.))
		eig1.setLeftBound(0.);		
	eig2 = ( m[0][0] + m[1][1] + sqrt(delta2) ) / 2. ;
	if(!(eig2 >= 0.))
		eig2.setLeftBound(0.);
}

//any dimension eigenvalue computation
//computes maximal eigenvalue for symmetric matrix m using the Gerschgorin algorithm implemented in CAPD
interval computeMaxEigPD(const IMatrix& m){
	interval eig1 = capd::matrixAlgorithms::Gershgorin< IMatrix, true >::maxEigenValueOfSymMatrix(m);
	if(!(eig1 >= 0.))
		eig1.setLeftBound(0.);	
	return eig1;
}

int main()
{

  ofstream fout1, fout2, fout3;
  fout1.open("direct_method.txt", std::ofstream::out);
  fout2.open("lagrangian_method.txt", std::ofstream::out);
  fout3.open("intersection.txt", std::ofstream::out);

  fout1.precision(12);
  fout2.precision(12);
  fout3.precision(12);
  cout.precision(12);

  try{

	//the inital box size
	const interval h = 0.1;

	const int DIM = 2;

	const interval TIMEHORIZON = 10;

	IVector x0(2);

	//Below I list the examples with the initial condition taken from Sayans approach code
	

	//Brusselator
    //IMap vectorField("var:x,y;fun: 1+x*x*y-2.5*x, 1.5*x-x*x*y;");
	//x0[0] = 1;	x0[1] = 1;


	//Inv vanderpol
	IMap vectorField("var:x,y; fun:-y, (x^2-1)*y+x;");
	x0[0] = 0.4;	x0[1] = 0.4;
		

	//Engine example
	//IMap vectorField("var:x,y;fun:-y-1.5*x^2-0.5*x^3,3*x-y;");
	//x0[0] = 0.2;	x0[1] = 0.2;	
	

	//the current initial condition box size (gets updated, when we replace the initial condition with the lagrangian ball)
	interval currentH = h;

	//this is the initial box of initial conditions, which is going to be rigorously integrated in time
	IVector fullSet(2);
	fullSet[0] = x0[0] + Interval(-rightBound(h), rightBound(h));
	fullSet[1] = x0[1] + Interval(-rightBound(h), rightBound(h));


    // the solver, is uses high order enclosure method to verify the existence
    // of the solution. The order is set to (see below).
    OdeSolver solver(vectorField,10);
    solver.setAbsoluteTolerance(1e-10);
    solver.setRelativeTolerance(1e-10);
	interval timeStep = 0.01;
	solver.setStep(timeStep);
	

	IMatrix grad(2,2) , cgm(2,2);

	int i;

	C1Rect2Set c1fullSet(fullSet);

	interval totalTime = 0.;

	//interval of time after which the data is outputted (has to be a multiple of the time-step)
	interval T( 1. * timeStep );

	for(i = 1; totalTime <= TIMEHORIZON; i++){
		
		c1fullSet.move(solver);

		totalTime += T;
		std::cout << "t=" << i*T << "\n"; //" set diameters=" << diam( (IVector)c1fullSet ) << "\n";

		IMatrix grad2 = (IMatrix)c1fullSet;	//this is the enclosure for the gradient of the flow with all initial conditions contained in the box 'fullSet'
		IMatrix cgm2 = transpose(grad2) * grad2; // the Cauchy-Green deformation matrices

		//compute eigenvalues of C-G matrix , computing rigorous eigenvalues not implemented in CAPD - computing explicitely
		//as zeros of the characteristic polynomial
		interval lambdaMax;
		lambdaMax = computeMaxEigPD( cgm2 );
		//lambdaMax is the maximal eigenvalue, therefore
		//the streching factor (stretch2) is the squareroot of lambdaMax	
		interval stretch2 = sqrt(lambdaMax);
		std::cout << "CG stretch fac=" << sqrt(stretch2) << "\n";
						

		//outputs the lagriangan method set (format timestep center_x_coord center_y_coord radius_of_the_ball)
		interval ballR = currentH * stretch2;
		fout2 << leftBound(totalTime) << " " << rightBound( c1fullSet.get_x()[0] ) << " " << rightBound( c1fullSet.get_x()[1] ) << " " << rightBound(ballR) << "\n";

		IVector currentSet = (IVector)c1fullSet;
		//outputs the one-step standard method (format timestep left_bd_x_coord right_bd_x_coord left_bd_y_coord right_bd_y_coord)
		fout1 << leftBound(totalTime) << " " << leftBound(currentSet[0]) << " " << rightBound(currentSet[0]) << " " << leftBound(currentSet[1]) << " " << rightBound(currentSet[1]) << "\n";

		//computes a representable enclosure for the ball from the lagrangian approach
		//and intersects it with the bounds from the one step method
		IVector lagrangianBds(2);
		//NONRIGOROUS ELEMENT intstead of c1fullSet.get_x(), which returns a point, rigorous bounds for the integrated centre of the ball should be used
		lagrangianBds[0] = c1fullSet.get_x()[0] + interval(-rightBound(ballR) , rightBound(ballR) );
		lagrangianBds[1] = c1fullSet.get_x()[1] + interval(-rightBound(ballR) , rightBound(ballR) );

		//computes the ratio of the volume of the lagrangian set (representable enclosure of a ball) vs the volume of the direct integration set
		interval lagrangianVol = 1., directVol = 1.;
		for(int j = 0; j < DIM; j++){
			lagrangianVol *= diam(lagrangianBds)[j];
			directVol *= diam((IVector)c1fullSet)[j];
		}
		std::cout << "lagrangianVol=" << lagrangianVol << ", directVol=" << directVol << ", ratio=" << (lagrangianVol / directVol) << "\n";

		if(LAGRANGIAN_UPDATE)
			//perform the Lagrange update
			if( (lagrangianVol / directVol) < 0.8 && stretch2 < 1.) {				
				c1fullSet = C1Rect2Set(lagrangianBds);
				//the initial condition is reinitialized to lagrangianBds, therefore the new h is oldH * streching fac
				currentH = currentH * stretch2;
				std::cout << "\nUPDATING BOUNDS NOW\nNew set is taken to be the lagrangian bounds.\nNew set=" << lagrangianBds << "\n";
			}

		std::cout << "\n";

		IVector intersect = intersection(lagrangianBds, currentSet);
		//outputs the data to the file fout3 (format timestep are_lagrangian_bounds_smaller_on_any_coordinate left_bd_x_coord right_bd_x_coord left_bd_y_coord right_bd_y_coord)
		fout3 << leftBound(totalTime) << " " << (lagrangianBds[0].subset(currentSet[0]) || lagrangianBds[1].subset(currentSet[1])) << " " <<
			leftBound(intersect[0]) << " " << rightBound(intersect[0]) << " " << leftBound(intersect[1]) << rightBound(intersect[1]) << "\n";


		//I put here example how to get the continuous reachtube for either of the approaches
		//for example enclosure for the interval [0, timeStep] is obtained like that
		const IOdeSolver::SolutionCurve& curve = solver.getCurve();
		interval domain = interval(0,1)*timeStep;
		IVector v = curve(domain);
		//(but we can obtain smaller pieces of the bounds as well)
		//analogously we can obtain bounds for the gradient of the flow like that
		IMatrix m = curve[domain];

	} // END for loop
  } catch(exception& e) {
    cout << "\n\nException caught!\n" << e.what() << endl << endl;
  }

	fout1.close();
	fout2.close();
	fout3.close();

} // END
